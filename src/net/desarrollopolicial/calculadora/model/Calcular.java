/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.desarrollopolicial.calculadora.model;

/**
 *
 * @author utudevelop
 */
public class Calcular {
    
    private Double Num1;
    private Double Num2;
    private Double Resultado;

    public String getSuma(){
        this.Resultado= this.Num1+this.Num2;
        return "La Suma es: "+ this.Resultado;
    }
    public String getResta(){
        this.Resultado= this.Num1-this.Num2;
        return "La Resta es: "+ this.Resultado;
    }
    public String getMult(){
        this.Resultado= this.Num1*this.Num2;
        return "La Multiplicación es: "+ this.Resultado;
    }
    public String getDiv(){
        this.Resultado= this.Num1/this.Num2;
        return "La División eS LA SIGUIENTE: "+ this.Resultado;
    }
    
    public Double getNum1() {
        return Num1;
    }

    public void setNum1(Double Num1) {
        this.Num1 = Num1;
    }

    public Double getNum2() {
        return Num2;
    }

    public void setNum2(Double Num2) {
        this.Num2 = Num2;
    }

    public Double getResultado() {
        return Resultado;
    }

    public void setResultado(Double Resultado) {
        this.Resultado = Resultado;
    }
    
    
}
