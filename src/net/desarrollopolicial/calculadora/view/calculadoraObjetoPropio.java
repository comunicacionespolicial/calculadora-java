package net.desarrollopolicial.calculadora.view;

import net.desarrollopolicial.calculadora.model.Calcular;
import net.desarrollosPoliciales.OperadorMatematico;
/**
 *
 * @author utudevelop
 */
public class calculadoraObjetoPropio extends javax.swing.JFrame {

    public calculadoraObjetoPropio() {
        initComponents();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        txtNum1 = new javax.swing.JTextField();
        txtNum2 = new javax.swing.JTextField();
        btnCalc = new javax.swing.JButton();
        lblResult = new javax.swing.JLabel();
        btnCalcMul = new javax.swing.JButton();
        btnCalcResta = new javax.swing.JButton();
        btnCalcDiv = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        btnCalc.setText("sumar");
        btnCalc.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCalcActionPerformed(evt);
            }
        });

        lblResult.setText("jLabel1");

        btnCalcMul.setText("multi");
        btnCalcMul.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCalcMulActionPerformed(evt);
            }
        });

        btnCalcResta.setText("restar");
        btnCalcResta.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCalcRestaActionPerformed(evt);
            }
        });

        btnCalcDiv.setText("div");
        btnCalcDiv.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCalcDivActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(27, 27, 27)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtNum2, javax.swing.GroupLayout.PREFERRED_SIZE, 333, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtNum1, javax.swing.GroupLayout.PREFERRED_SIZE, 333, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblResult)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(btnCalc)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnCalcResta)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnCalcMul)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnCalcDiv)))
                .addContainerGap(40, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(46, 46, 46)
                .addComponent(txtNum1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtNum2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(12, 12, 12)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnCalc)
                    .addComponent(btnCalcMul)
                    .addComponent(btnCalcResta)
                    .addComponent(btnCalcDiv))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(lblResult)
                .addContainerGap(124, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    Calcular cc;

    private boolean tomarDatos() {
        try {
            cc = new Calcular();
    
            cc.setNum1(Double.parseDouble(txtNum1.getText()));
            cc.setNum2(Double.parseDouble(txtNum2.getText()));
            
            return true;
        } catch (NumberFormatException e) {
            mostrarResultado(e.getMessage());
            return false;
        }
    }

    private void mostrarResultado(String msg) {
        lblResult.setText(msg);
        txtNum1.setText(""+cc.getResultado());
        txtNum1.setEnabled(false);
        txtNum2.setText("");
        txtNum2.requestFocus();
    }

    private void btnCalcActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCalcActionPerformed
        // TODO add your handling code here:
        if (tomarDatos()) {
            mostrarResultado(cc.getSuma());
        }
    }//GEN-LAST:event_btnCalcActionPerformed

    private void btnCalcMulActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCalcMulActionPerformed
        // TODO add your handling code here:
        if (tomarDatos()) {
            mostrarResultado(cc.getMult());
        }
    }//GEN-LAST:event_btnCalcMulActionPerformed

    private void btnCalcRestaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCalcRestaActionPerformed
        // TODO add your handling code here:
        if (tomarDatos()) {
            mostrarResultado(cc.getResta());
        }
    }//GEN-LAST:event_btnCalcRestaActionPerformed

    private void btnCalcDivActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCalcDivActionPerformed
        // TODO add your handling code here:
        if (tomarDatos()) {
            mostrarResultado(cc.getDiv());
        }
    }//GEN-LAST:event_btnCalcDivActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(calculadoraObjetoPropio.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(calculadoraObjetoPropio.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(calculadoraObjetoPropio.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(calculadoraObjetoPropio.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new calculadoraObjetoPropio().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCalc;
    private javax.swing.JButton btnCalcDiv;
    private javax.swing.JButton btnCalcMul;
    private javax.swing.JButton btnCalcResta;
    private javax.swing.JLabel lblResult;
    private javax.swing.JTextField txtNum1;
    private javax.swing.JTextField txtNum2;
    // End of variables declaration//GEN-END:variables
}
